#!/bin/bash

sudo yum -y install httpd
if rpm -qa | grep '^httpd-[0-9]' >/dev/null 2>&1
then
    sudo systemctl start httpd
else
    exit 1
fi


sudo sh -c "cat >/var/www/html/index.html <<_END_
<h1>WOW</h1>
<img src=\"https://media.giphy.com/media/5VKbvrjxpVJCM/giphy.gif\">
_END_"

