# Jenkins CI exercise

## Jenkins ( CI/CD)
- Automation server -> Should be locked down to personal IP 
- Will be linked to AWS
- Make it run tasks for you -> Do whatever you define as tasks 
- Can run bash and therefore can run a lot of things
- To get Jenkins talking to a repo
    - Need to make a new key and save it as a new name 
    - Put private key onto Jenkins and public key onto repo platform 


## Exercise
- Get machine from Jenkins server to new AWS machine 
  

## Changes for 27th April 2020
- Changed which branch that Jenkins is tracking 